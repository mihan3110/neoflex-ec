package stringutils;

public class StringUtil {

    public static String reverseString(String s){

        return new StringBuilder(s).reverse().toString();
    }
}
