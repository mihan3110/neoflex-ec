import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import servs.MainServlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class MainServletTest extends HttpServlet {

    @Test
    public void testMainServlet() throws IOException, ServletException {

        MainServlet servlet = new MainServlet();
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        when(req.getParameter("qwer")).thenReturn("rewq");
        assertEquals("rewq", req.getParameter("qwer"));

        when(req.getParameter("qwerty")).thenReturn("ytrewq");
        assertEquals("ytrewq", req.getParameter("qwerty"));

        when(req.getParameter("q123")).thenReturn("321q");
        assertEquals("321q", req.getParameter("q123"));

        when(req.getParameter("q1234")).thenReturn("4321q");
        assertEquals("4321q", req.getParameter("q1234"));

       // servlet.doGet(req, res);

       // assertTrue(stringWriter.toString().contains("My expected string"));




    }

}
